from rich import print
from googleapiclient.discovery import build
import google.auth

# If modifying these scopes, delete the file token.json.
SCOPES = [
    "https://www.googleapis.com/auth/documents",
    "https://www.googleapis.com/auth/drive",
]

# The ID of a sample document.
DOCUMENT_ID = "12NFtspNtF0w2_aNzPv6XtzvN1lCEV9I61DexnvkBcNQ"


creds, _ = google.auth.default(scopes=SCOPES)

service = build("docs", "v1", credentials=creds)
# Retrieve the documents contents from the Docs service.
# document = service.documents().get(documentId=DOCUMENT_ID).execute()

# print(document["body"]["content"])

# create new doc (google doc)

title = "OCR output"
body = {"title": title}
# doc = service.documents().create(body=body).execute()


# insert result (doc)
# doc_id = doc["documentId"]
# print(f"https://docs.google.com/document/d/{doc_id}/edit")

with open("01.txt") as f:
    data = f.read()

requests = [
    {
        "insertText": {
            "location": {
                "index": 1,
            },
            "text": data,
        }
    }
]


(
    service.documents()
    .batchUpdate(documentId=DOCUMENT_ID, body={"requests": requests})
    .execute()
)

# share and make user owner (drive API)
